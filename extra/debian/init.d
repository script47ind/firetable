#!/bin/bash
#
# Firetable start/stop script for Linux
#
### BEGIN INIT INFO
# Provides:          firetable
# Required-Start:    $syslog $network $remote_fs
# Required-Stop:     $syslog $network $remote_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: IPtables firewall script
# Description:       Firetable, a script for IPtable firewalls
### END INIT INFO

NAME=hiawatha
SCRIPT=/usr/sbin/firetable

# /etc/init.d/hiawatha: start and stop the Hiawatha webserver daemon

test -x $SCRIPT || exit 0
( ${SCRIPT} -\v 2>&1 | grep -q Firetable ) 2>/dev/null || exit 0

# Defaults
if [ -r /etc/firetable/firetable.conf ]; then 
	INTERFACES=`grep "^enable_on_boot" /etc/firetable/firetable.conf | cut -f2 -d"=" | sed "s/^ *//"`
fi

function firetable {
	if [ "${INTERFACES}" = "all" ]; then
		${SCRIPT} $1
	elif [ "${INTERFACES}" != "" ]; then
		${SCRIPT} $1 ${INTERFACES}
	fi
}

case "$1" in
	start|stop)
		firetable $1
		;;

	restart)
		firetable stop
		firetable start
		;;

    *)
        log_action_msg "Usage: /etc/init.d/firetable {start|stop|restart}" || true
        exit 1
esac

exit 0
